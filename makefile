COMPOSE_FILE_PATH := -f docker-compose.yml

#backend: docker-compose.yml
#	docker-compose up -d backend

up: docker-compose.yml
	docker-compose up -d

down: docker-compose.yml
	docker-compose down

backend: docker-compose.yml
	docker-compose up -d backend

frontend: docker-compose.yml
	docker-compose up -d frontend

down-backend: docker-compose.yml
	docker-compose down -d backend

down-frontend: docker-compose.yml
	docker-compose down -d frontend


clean: docker-compose.yml
	docker-compose down
 
stop:
	$(info Make: Stopping environment containers.)
	@docker-compose stop

