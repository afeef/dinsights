let express = require('express');
const port = process.env.Port || 3000;

const app = express();

app.get('/',(req, res) => {
    res.send("This is a developer insight app");
});

app.listen(port, () => {
    console.log("App listening at Port:", port);
});
